extends HBoxContainer

@onready var rotation_label = $VBoxContainer/Rotation


func _on_slider_value_changed(value):
	rotation_label.text = "%s Degrees" % value
