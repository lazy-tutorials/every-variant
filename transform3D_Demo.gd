@tool
extends Node3D

@onready var origin := $Origin
@onready var x := $Origin/X
@onready var y := $Origin/Y
@onready var z := $Origin/Z
@onready var object := $Object
@export var update := false


func _process(_delta):
	if update:
		object.transform = Transform3D(
				x.position,
				y.position,
				z.position,
				origin.position)
