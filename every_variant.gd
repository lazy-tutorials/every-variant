extends Node
@export_category("Every Variant in Godot")
@export_group("Basic Variants")

@export var variant := 0

@export var integer: int = 10
@export_range(-1000000,1000000,0.00000001) var floating_point: float = 10.0
@export var boolean: bool = false
@export var color: Color
@export var string: String = "Hello!"
@export var string_name: StringName = &"Hello"
@export var node_path: NodePath = ^"Button"

@export_subgroup("Vectors")
@export var vector2: Vector2
@export var vector2i: Vector2i
@export var vector3: Vector3
@export var vector3i: Vector3i
@export var vector4: Vector4
@export var vector4i: Vector4i

@export_group("Collections")
@export var array: Array = []
@export var int_array: Array[int] = []
@export var array_array: Array[Array] 

@export var dictionary: Dictionary = {
	"Red": Color.RED,
	"Purple Thing": Color.REBECCA_PURPLE,
	"Blue": Color.CADET_BLUE
}

@export_subgroup("Packed Arrays")
@export var packed_byte_array: PackedByteArray
@export var packed_color_array: PackedColorArray
@export var packed_float32_array: PackedFloat32Array
@export var packed_float64_array: PackedFloat64Array
@export var packed_int32_array: PackedInt32Array
@export var packed_int64_array: PackedInt64Array
@export var packed_string_array: PackedStringArray
@export var packed_vector2_array: PackedVector2Array
@export var packed_vector3_array: PackedVector3Array

@export_group("Objects and Signals")
@export var resource_id: RID
signal did_thing
signal did_other_thing
@export var callable: Callable
@export var my_signal: Signal

@export_group("Transformations and Rotations")
@export var rect2: Rect2
@export var rect2i: Rect2i
@export var transform_2D: Transform2D
@export var aabb: AABB
@export var basis: Basis
@export var transform_3D: Transform3D
@export var plane: Plane
@export var quaternion: Quaternion
@export var projection: Projection


func _ready():
	print(type_string(typeof(get_node(node_path))))
	
	emit_signal("did_thing")
	
	connect("did_other_thing", _did_other_thing)
	
	emit_signal("did_other_thing")
	
	callable = _did_a_third_thing
	callable.call()
	
	
func _did_thing():
	print("Did a thing.")


func _did_other_thing():
	print("Did another thing.")
	
	
func _did_a_third_thing():
	print("My creativity knows no bounds!")


func _on_did_thing():
	pass # Replace with function body.
