extends Node2D

@export var string1: String
@export var string2: String

@export var string_name1: StringName
@export var string_name2: StringName


func _ready():
	# Test with String
	var start_time = Time.get_unix_time_from_system()
	for i in range(100000):
		var result = string1 == string2
	var end_time = Time.get_unix_time_from_system()

	print("Time taken for String comparison: " + str((end_time - start_time) * 1000) + " ms")

	# Test with StringName
	start_time = Time.get_unix_time_from_system()
	for i in range(100000):
		var result = string_name1 == string_name2
	end_time = Time.get_unix_time_from_system()

	print("Time taken for StringName comparison: " + str((end_time - start_time) * 1000) + " ms")
