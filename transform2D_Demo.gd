@tool
extends Node2D

@onready var rect_2_origin := $Rect2_Origin
@onready var rect_2_width_height := $Rect2_Origin/Rect2_Width_Height
@onready var transform_2d_origin := $Transform2D_Origin
@onready var transform_2d_x := $Transform2D_Origin/Transform2D_X
@onready var transform_2d_y := $Transform2D_Origin/Transform2D_Y
@onready var rectangle := $Rectangle
@onready var sprite := $Sprite
@export var update := false


func _process(_delta):
	if update:
		rectangle.set_position(rect_2_origin.position)
		rectangle.set_size(rect_2_width_height.position)
		
		sprite.transform = Transform2D(transform_2d_x.position / 64.0, transform_2d_y.position / 64.0, transform_2d_origin.position)
