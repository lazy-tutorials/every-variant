extends Node3D

@onready var x = $X
@onready var z = $X/Z
@onready var y = $X/Z/Y
@onready var gimbal_box = $GimbalBox

@onready var x_slider = $"Euler Panel/MarginContainer/VBoxContainer/XBox/VBoxContainer/XSlider"
@onready var z_slider = $"Euler Panel/MarginContainer/VBoxContainer/ZBox/VBoxContainer/ZSlider"
@onready var y_slider = $"Euler Panel/MarginContainer/VBoxContainer/YBox/VBoxContainer/YSlider"

@onready var i = $"Quat Panel/MarginContainer/VBoxContainer/QuatControls/i"
@onready var j = $"Quat Panel/MarginContainer/VBoxContainer/QuatControls/j"
@onready var k = $"Quat Panel/MarginContainer/VBoxContainer/QuatControls/k"

@onready var y_gimbal = $"Euler Panel/MarginContainer/VBoxContainer/Toggles/YGimbal"
@onready var z_gimbal = $"Euler Panel/MarginContainer/VBoxContainer/Toggles/ZGimbal"
@onready var cube = $"Euler Panel/MarginContainer/VBoxContainer/Toggles/Cube"

@onready var up_vector = $Quats/Box/UpVector
@onready var right_vector = $Quats/Box/RightVector
@onready var front_vector = $Quats/Box/FrontVector

@onready var quat_box = $Quats/Box
@onready var quat_basis = $Quats/QuatBasis
@onready var quat_label = $"Quat Panel/MarginContainer/VBoxContainer/Quat_label"

@onready var w_label = $"Quat Panel/MarginContainer/VBoxContainer/QuatControls/VBoxContainer/W_Label"
@export var raw_ijk := Vector3(1, 0, 0)
@export var ijk: Vector3
@export var raw_w: float
@export var quat: Quaternion
@export var final_rotation: Quaternion
@export var rotation_speed: float = 1.0  # Adjust this for faster or slower rotation

var is_rotating := false


# Euler rotations:
func _on_x_slider_value_changed(value):
	x.rotation_degrees.x = value


func _on_y_slider_value_changed(value):
	# Why do I need to rotate X in order to rotate Y? Because Euler angles are confusing.
	y.rotation_degrees.x = value - 90


func _on_z_slider_value_changed(value):
	z.rotation_degrees.z = value - 90


# Quaternion Rotations:
func update_quat():
	if raw_ijk.length() > 0:
		ijk = raw_ijk.normalized()
		
		quat = Quaternion(ijk, deg_to_rad(raw_w))
		quat_label.text = "Rotation Quaternion: " + str(quat)
		
		# Point the arrow in the right direction.
		var rotation = Quaternion(Vector3.RIGHT, ijk)
		quat_basis.quaternion = rotation


func _on_rotate_pressed():
	if is_rotating:
		quat_box.quaternion = final_rotation # Finish the previous rotation.
	is_rotating = true
	final_rotation = (quat * quat_box.quaternion).normalized()


func _process(delta):
	if is_rotating:
		var current_quat: Quaternion = quat_box.quaternion
		var interpolated_quat := current_quat.slerp(final_rotation, delta * rotation_speed)
		
		if interpolated_quat.angle_to(quat_box.quaternion) < 0.00001:
			is_rotating = false
			interpolated_quat = final_rotation
		quat_box.quaternion = interpolated_quat


func _on_i_value_changed(value):
	raw_ijk.x = value
	update_quat()


func _on_j_value_changed(value):
	raw_ijk.y = value
	update_quat()


func _on_k_value_changed(value):
	raw_ijk.z = value
	update_quat()


func _on_w_value_changed(value):
	raw_w = value
	w_label.text = "w: %s Degrees" % raw_w
	update_quat()


func _on_normalize_pressed():
	i.value = ijk.x
	j.value = ijk.y
	k.value = ijk.z


# Visibility and reset options:
func _on_up_toggled(toggled_on):
	up_vector.visible = toggled_on


func _on_forward_toggled(toggled_on):
	front_vector.visible = toggled_on


func _on_right_toggled(toggled_on):
	right_vector.visible = toggled_on


func _on_rotation_toggled(toggled_on):
	quat_basis.visible = toggled_on


func _on_zero_pressed():
	raw_ijk = Vector3.ZERO
	ijk = Vector3.ZERO
	i.value = ijk.x
	j.value = ijk.y
	k.value = ijk.z


func _on_x_gimbal_toggled(toggled_on):
	x.visible = toggled_on
	y_gimbal.disabled = !toggled_on
	z_gimbal.disabled = !toggled_on


func _on_z_gimbal_toggled(toggled_on):
	z.visible = toggled_on
	y_gimbal.disabled = !toggled_on


func _on_y_gimbal_toggled(toggled_on):
	y.visible = toggled_on


func _on_cube_toggled(toggled_on):
	gimbal_box.visible = toggled_on


func _on_gimbal_zero_pressed():
	x_slider.value = 0
	y_slider.value = 0
	z_slider.value = 0
